let votes = {};


exports.getVotes = function(){
  return votes
};

exports.getVote = function(voteId){
  return votes[voteId]
};

exports.addVote = function(voteId, vote){
  votes[voteId] = vote
};
