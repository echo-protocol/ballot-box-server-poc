const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const multer = require('multer'); // v1.0.5
const upload = multer(); // for parsing multipart/form-data
const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;

const auth =  require('./services/authentication');
const Vote = require('./services/vote');
const votes = require('./votes');

app.use(bodyParser.json()); // for parsing application/json

app.post('/vote', upload.array(), function (req, res) {

  if(!auth.authenticateAgoraService(req.body.serverKey)) res.status(401).end();

  const vote = new Vote(req.body.pubKey, req.body.choicesCount, req.body.voteStart, req.body.voteDuration, req.body.authorizedVoters);
  votes.addVote(vote.voteId, vote);
  res.set('Content-Type', 'application/json');

  res.send({
    voteId: vote.voteId,
    choicesList: vote.choicesList,
    votesCount: vote.votesCount
  });

  console.log('New vote instantiated: ' + vote.voteId)
});

app.post('/cast', upload.array(), async function (req, res) {

  const vote = votes.getVote(req.body.voteId);

  console.log('CAST BALLOT # ' + (vote.aggregatedVotes.counter + 1).toString() + ' FOR VOTE ' + req.body.voteId + '\n');

  if(vote.tally) {
    const result = { status: false, reason: 0 };

    console.log("BALLOT REJECTED: VOTE CLOSED\n");

    res.send(result);

  } else {
    const c1 = new BigInt(req.body.c1, 10);
    const c2 = new BigInt(req.body.c2, 10);
    const nipokek = {
      aBigInt: new BigInt(req.body.nipokek.aBigInt, 10),
      eBigInt: new BigInt(req.body.nipokek.eBigInt, 10),
      zBigInt: new BigInt(req.body.nipokek.zBigInt, 10)
    };

    let commitments = [];
    let challenges = [];
    let responses = [];

    for(let i = 0; i < req.body.nidzkpop.commitments.length; i++){
      commitments.push(new BigInt(req.body.nidzkpop.commitments[i], 10));
      challenges.push(new BigInt(req.body.nidzkpop.challenges[i], 10));
      responses.push(new BigInt(req.body.nidzkpop.responses[i], 10));
    }
    const nidzkpop = {
      commitments,
      challenges,
      responses
    };

    const result = await vote.castBallot(req.body.userId, c1, c2, nipokek, nidzkpop);

    res.send(result)
  }
});

app.get('/votes', function(req, res) {

  let votesInfo = votes.getVotes();

  let body = {};

  Object.keys(votesInfo).forEach((vote) => {

    body[vote] = {};
    body[vote].voteStart = votesInfo[vote].voteStart;
    body[vote].voteId = votesInfo[vote].voteId;
    body[vote].choicesList = votesInfo[vote].choicesList;
    body[vote].votesCount = votesInfo[vote].votesCount;
    body[vote].voteDuration = votesInfo[vote].voteDuration;
    body[vote].voteStart = votesInfo[vote].voteStart;
    body[vote].tally = votesInfo[vote].tally;
    body[vote].pubKey = {
      p: votesInfo[vote].pubKey.p.toString(10),
      g: votesInfo[vote].pubKey.g.toString(10),
      A: votesInfo[vote].pubKey.A.toString(10)
    }
  });

  res.send(body)
});

app.post('/tally', upload.array(), async function (req, res) {

  if(!auth.authenticateAgoraService(req.body.serverKey)) res.status(401).end();

  const vote = votes.getVote(req.body.voteId);

  const tally = await vote.performTally(req.body.decryptionKey);

  res.send(tally)

});

app.listen(3001, () => {
  console.log('Ballot Box Server Listening on port 3000\n')
});
