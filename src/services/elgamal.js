const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;

exports.decrypt = async function (c1, c2, skBigInt, gBigInt, pBigInt){

  return new Promise(async (resolve, reject) => {

    try{

      console.log("AGGREGATED VOTE: ", c1.toString(10), c2.toString(10));

      let decryptedAggregatedVoteBigInt = c1.modPow(skBigInt, pBigInt).modInverse(pBigInt).multiply(c2).remainder(pBigInt);

      resolve(decryptedAggregatedVoteBigInt);

    } catch(e){ reject(e) }
  })
}
