const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;
const Utils = require('../utils/utils');
const SHA3 = require('../utils/sha3');


exports.verifyPOKOfEphemeralKey = function (pubKey, c1BigInt, nipokek){ // }, aBigInt, eBigInt, zBigInt, gBigInt, c1BigInt, pBigInt){
  return new Promise(async (resolve, reject) => {
    try{

      const stringToHash = nipokek.aBigInt.toString(16) + c1BigInt.toString(16);

      const result1 = nipokek.eBigInt.equals(new BigInt(SHA3.sha3(stringToHash), 16).remainder(pubKey.p)); // VERIFY THAT CHALLENGE e WAS BUILT CORRECTLY

      const result2 = pubKey.g.modPow(nipokek.zBigInt, pubKey.p).equals(nipokek.aBigInt.multiply(c1BigInt.modPow(nipokek.eBigInt, pubKey.p)).remainder(pubKey.p)); // VERIFY DHT

      if(result1 && result2){
        console.log("SUCCESSFULLY VERIFIED POK of k\n");
      } else {
        console.log("POK of k VERIFICATION FAILED !!!");
        console.log(result1, result2);
      }

      resolve(result1 && result2)

    } catch(e) { reject(e) }
  })
};


exports.verifyDisjunctiveProofOfPlaintext = function (pubKey, validPlainTexts, c2BigInt, nidzkpop){

  let validPlainTextsBigInt = [];
  validPlainTexts.forEach((plaintext) => {
    validPlainTextsBigInt.push(new BigInt(plaintext.toString(), 10))
  });



  /*** VERIFY INDIVIDUAL PROOFS ***/

  let result1 = true;
  for(let i =0; i < validPlainTextsBigInt.length; i++){

    let leftBigInt = pubKey.A.modPow(nidzkpop.responses[i], pubKey.p);

    let rightBigInt = nidzkpop.commitments[i].multiply(c2BigInt.multiply(validPlainTextsBigInt[i].modInverse(pubKey.p)).modPow(nidzkpop.challenges[i].abs(), pubKey.p)).remainder(pubKey.p);

    let result = leftBigInt.equals(rightBigInt);

    if(!result){
      result1 = false;
    }

  }

  /*** COMPUTE EXPECTED CHALLENGE ***/

  let stringToHash = "";
  nidzkpop.commitments.forEach((commitment) => {
    stringToHash += commitment.toString(10);
  });

  const eEBigInt = new BigInt(SHA3.sha3(stringToHash), 16).remainder(pubKey.p); // CHALLENGE e

  let sumChallenges = nidzkpop.challenges[0];
  for(let i = 1; i < nidzkpop.challenges.length; i++){
    sumChallenges = sumChallenges.add(nidzkpop.challenges[i]);
  }

  /*** VERIFY CHALLENGES ***/

  let result2 = eEBigInt.equals(sumChallenges.remainder(pubKey.p));

  if(result1 && result2){
    console.log("SUCCESSFULLY VERIFIED DISJUNCTIVE ZKP OF PLAINTEXT\n");
  } else {
    console.log("DISJUNCTIVE ZKP OF PLAINTEXT VERIFICATION FAILED !!!");
    console.log(result1, result2);
  }
  return(result1 && result2)
};
