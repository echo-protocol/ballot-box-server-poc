require('dotenv').config();

exports.authenticateAgoraService = function(key){
  return key === process.env.AGORA_SERVICE_KEY;
};
