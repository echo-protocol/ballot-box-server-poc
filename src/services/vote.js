const uuidv4 = require('uuid/v4');
const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;
const proofs = require('./proofs');
const elgamal = require('./elgamal');
const utils = require('../utils/utils');

const validPlainTexts = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29];

class Vote {

  constructor(pubKey, choicesCount, voteStart, voteDuration, authorizedVoters) {
    this.voteStart = voteStart;
    this.voteId = uuidv4();
    this.choicesList = validPlainTexts.slice(0, choicesCount);
    this.authorizedVoters = authorizedVoters;
    this.voteDuration = voteDuration;
    this.pubKey = { p: new BigInt(pubKey.p, 10), g: new BigInt(pubKey.g, 10), A: new BigInt(pubKey.A, 10) };
    this.maxAggregatedVotesCount = utils.computeMaxNbAggregatedVotes(new BigInt(this.choicesList[choicesCount - 1].toString(), 10), this.pubKey.p.bitLength());
    this.aggregatedVotes = {
      aggregatedVotes: [{
        C1: new BigInt('1', 10),
        C2: new BigInt('1', 10),
      }],
      counter: 0,
    };
    this.tally = null;
  }


  async castBallot(userId, c1, c2, nipokek, nidzkpop){

    if(!this.authorizedVoters[userId]) {

      console.log("BALLOT REJECTED: VOTER AUTHENTICATION FAILED\n");

      return {
        status: false,
        reason: 1
      };
    }

    const verifyPOKEK = await proofs.verifyPOKOfEphemeralKey(this.pubKey, c1, nipokek);

    if(!verifyPOKEK) {

      console.log("BALLOT REJECTED: POKEK VERIFICATION FAILED\n");

      return {
        status: false,
        reason: 2
      };
    }

    const verifyDZKPOP = await proofs.verifyDisjunctiveProofOfPlaintext(this.pubKey, this.choicesList, c2, nidzkpop);

    if(!verifyDZKPOP) {

      console.log("BALLOT REJECTED: DZKPOP VERIFICATION FAILED\n");

      return {
        status: false,
        reason: 3
      };
    }

    this.aggregateVote(c1, c2);

    console.log('BALLOT CAST SUCCESSFULLY\n');

    return {
      status: true
    }

  };

  async performTally(privKey){
    this.privKey = new BigInt(privKey, 10);
    const decryptedAggregatedVoteBigInt = await this.decryptAggregatedVotes(this.aggregatedVotes.aggregatedVotes, this.privKey, this.pubKey.g, this.pubKey.p, null, 0);
    this.tally = this.countVotes(this.choicesList, decryptedAggregatedVoteBigInt);
    return this.tally
  };

  aggregateVote(c1, c2){
      const aggregatesCount = this.aggregatedVotes.aggregatedVotes.length;
      this.aggregatedVotes.aggregatedVotes[aggregatesCount - 1].C1
        = this.aggregatedVotes.aggregatedVotes[aggregatesCount - 1].C1.multiply(c1);
      this.aggregatedVotes.aggregatedVotes[aggregatesCount - 1].C2
        = this.aggregatedVotes.aggregatedVotes[aggregatesCount - 1].C2.multiply(c2);

      this.aggregatedVotes.counter += 1;

      if(this.aggregatedVotes.counter >= this.maxAggregatedVotesCount){
        this.aggregatedVotes.aggregatedVotes.push({
          C1: new BigInt('1', 10),
          C2: new BigInt('1', 10),
        });
        this.aggregatedVotes.counter = 0;
      }
  }



   async decryptAggregatedVotes(aggregatedVotesList, skBigInt, gBigInt, pBigInt, decryptedAggregatedVoteBigInt, index){
    return new Promise(async (resolve, reject) => {
      try{

        if(index < aggregatedVotesList.length){

          if(!decryptedAggregatedVoteBigInt){

            decryptedAggregatedVoteBigInt = await elgamal.decrypt(aggregatedVotesList[index].C1, aggregatedVotesList[index].C2, skBigInt, gBigInt, pBigInt);

          } else {

            decryptedAggregatedVoteBigInt = decryptedAggregatedVoteBigInt.multiply(await elgamal.decrypt(aggregatedVotesList[index].C1, aggregatedVotesList[index].C2, skBigInt, gBigInt, pBigInt));

          }

          resolve(this.decryptAggregatedVotes(aggregatedVotesList, skBigInt, gBigInt, pBigInt, decryptedAggregatedVoteBigInt, index + 1))

        } else {

          console.log("\nDECRYPTED AGGREGATED VOTE: ", decryptedAggregatedVoteBigInt.toString(10), "\n");

          resolve(decryptedAggregatedVoteBigInt);

        }

      } catch(e){ reject(e) }
    })
  }



  countVotes(validPlainTexts, decryptedAggregatedVoteBigInt){

    let votesCount = [];

    validPlainTexts.forEach((plainText) => {

      const plainTextBigInt = new BigInt(plainText.toString(), 10);

      let i = 0;

      while (decryptedAggregatedVoteBigInt.gcd(plainTextBigInt).equals(plainTextBigInt)) {

        decryptedAggregatedVoteBigInt = decryptedAggregatedVoteBigInt.divide(plainTextBigInt);

        i += 1

      }

      votesCount.push(i);

    });

    console.log("VOTE RESULTS:\n", votesCount);

    console.log('\n');

    return votesCount;

  }


}

module.exports = Vote;
